# Introduction

Cet exemple est le plus basique qui soit pour avoir une execution (ou
un binaire) qui ne fait rien.

Pour le tester:

Execution immédiate (compile, link, run et supprimer)
`go run main.go`

ou

`go build && ./v0`

# Ce qui est abordé

- Structure arborescente typique.
- Notion de package/module
- point d'entrée main


C larsonneur
