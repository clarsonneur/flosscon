package main

import "fmt"

var aGlobalString string

func main() {
	aGlobalString = "hello world!\n"
	var aLocalString = aGlobalString
	fmt.Print(aLocalString)
	myString := "Salut le monde!\n"
	fmt.Print(myString)
}
