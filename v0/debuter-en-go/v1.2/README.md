# Introduction

Cette fois, on aborde un module avancé pour lire des fichiers en yaml

Il se base sur un module disponible en externe (github)
On devra exploiter l'instruction `go get`

`go get gopkg.in/yaml.v2`

Vous verrez apparaitre un répertoire gopkg.in sous $GOPATH/src et
$GOPATH/pkg

Pour le tester:

Execution immédiate (compile, link, run et supprimer)
`go run main.go`

ou

`go build && ./v0`

# ce qui est abordé

- Le module yaml de Canonical.


C larsonneur
