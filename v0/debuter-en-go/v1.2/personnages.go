package main

import (
	"fmt"
	"os"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type Personnages struct {
	personnes map[string]*Contact
	Personnes []*Contact
}

func (p *Personnages) Person(name string) (*Contact){
	if v, found := p.personnes[name] ; !found {
		fmt.Printf("Unable to find person '%s'\n", name)
		os.Exit(1)
		return nil
	} else {
		return v
	}
}

func (p *Personnages) Init(filename string) {
	p.personnes = make(map[string]*Contact)

	var data []byte
	if d, err := ioutil.ReadFile(filename) ; err != nil {
		fmt.Print(err)
		os.Exit(1)
	} else {
		data = d
	}
	if err := yaml.Unmarshal(data, &p) ; err != nil {
		fmt.Print(err)
		os.Exit(1)
	}

	fmt.Printf("%d personnes chargées à partir de '%s'.", len(p.Personnes), filename)
	for _, contact := range p.Personnes {
		p.personnes[contact.Name] = contact
	}
	p.Personnes = nil
}
