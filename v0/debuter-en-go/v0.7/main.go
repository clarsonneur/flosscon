package main

import "fmt"

const (
	Juliette = "Juliette"
	Romeo = "Roméo"
)


type Contacts map[string]*Contact


func main() {
	Personnes := []string{Juliette, Romeo}
	Tels := []string{"+33653747395", "+33652235427"}
	var Personnages Contacts = make(map[string]*Contact)

	for index, name := range Personnes {
		var unContact *Contact
		Personnages[name] = unContact.DefinirPersonnages(name, Tels[index])
	}

	fmt.Print("\n\nRoméo et Juliette - Moderne:\n\n")

	romeoMsg := "Appelle moi au N°%s avant que j'escalade... \n\nTon %s."
	Personnages[Romeo].Message(romeoMsg, Personnages[Romeo].Tel, Personnages[Romeo].Name)

	julietteMsg := "Ca sonne occupé!!! tu téléphones à qui??? \n\nTa %s."
	Personnages[Juliette].Message(julietteMsg, Personnages[Juliette].Name)

}
