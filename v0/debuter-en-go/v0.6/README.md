# Introduction

Cet exemple montre une évolution montrant le passage de paramètres variable


Pour le tester:

Execution immédiate (compile, link, run et supprimer)
`go run main.go`

ou

`go build && ./v0`

# ce qui est abordé

- Le Hash
- Les fonctions attachés à une structure simili objet.
- Notion de pointeurs
- Passage de paramètres variable
- Utilisation d'interface{}

C larsonneur
