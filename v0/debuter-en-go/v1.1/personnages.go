package main

import (
	"fmt"
	"os"
	"strings"
	"bufio"
	"log"
)

type Personnages struct {
	Personnes map[string]*Contact
}

func (p *Personnages) Person(name string) (*Contact){
	if v, found := p.Personnes[name] ; !found {
		fmt.Printf("Unable to find person '%s'\n", name)
		os.Exit(1)
		return nil
	} else {
		return v
	}
}

func (p *Personnages) Init(filename string) {
	p.Personnes = make(map[string]*Contact)

	if fd, err := os.Open(filename) ; err != nil {
		fmt.Print(err)
	} else {
		defer fd.Close()

		scanner := bufio.NewScanner(fd)
		for scanner.Scan() {
			var unContact *Contact
			line := scanner.Text()
			if line == "" {
				continue
			}
			fields := strings.Split(line, ",")
			p.Personnes[fields[0]] = unContact.DefinirPersonnages(fields[0], fields[1])
		}
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
	}
}
