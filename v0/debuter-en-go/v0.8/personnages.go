package main

import (
	"fmt"
	"os"
)

type Personnages struct {
	Personnes map[string]*Contact
}

func (p *Personnages) Person(name string) (*Contact){
	if v, found := p.Personnes[name] ; !found {
		fmt.Printf("Unable to find person '%s'\n", name)
		os.Exit(1)
		return nil
	} else {
		return v
	}
}

func (p *Personnages) Init(personnes, tels []string) {
	p.Personnes = make(map[string]*Contact)

	for index, name := range personnes {
		var unContact *Contact
		p.Personnes[name] = unContact.DefinirPersonnages(name, tels[index])
	}

}
