package main

type Contact struct {
	Name string
	Tel  string
}

func (c *Contact)DefinirPersonnages(name, tel string) {
	c.Name = name
	c.Tel = tel
}
