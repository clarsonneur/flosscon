# Introduction

Cet exemple montre une évolution montrant la notion d'objet à la GO.


Pour le tester:

Execution immédiate (compile, link, run et supprimer)
`go run main.go`

ou

`go build && ./v0`

# ce qui est abordé

- Les fonctions attachés à une structure simili objet.
- Notion de pointeurs

C larsonneur
