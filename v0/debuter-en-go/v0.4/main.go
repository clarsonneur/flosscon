package main

import "fmt"

type Contact struct {
	Name string
	Tel string
}

const Juliette = "Juliette"

func DefinirPersonnages(name, tel string) (ret Contact) {
	ret.Name = name
	ret.Tel = tel
	return
}

func Message(qui Contact, message string) {
	fmt.Printf("%s:\n%s\n----\n", qui.Name, message)
}

func main() {
	juliette := DefinirPersonnages(Juliette, "+3365374739543")

	romeo := DefinirPersonnages("Roméo", "+33652235427")

	fmt.Print("\n\nRoméo et Juliette - Moderne:\n\n")

	romeoMsg := fmt.Sprintf("Appelle moi au N°%s avant que j'escalade... \n\nTon %s.", romeo.Tel, romeo.Name)
	Message(romeo, romeoMsg)

	julietteMsg := fmt.Sprintf("Ca sonne occupé!!! tu téléphones à qui??? \n\nTa %s.", juliette.Name)
	Message(juliette, julietteMsg)

}
