# Introduction

Illustration du workshop "Ecrire en GO"

la présentation se situe sous prez/


# Pré-requis à l'usage des exemples de ce repository git.

Il vous suffit d'avoir un linux, windows ou macos pour installer GO

Consultez le site https://golang.org/dl/

Ensuite, vous devez positionner ceci:

- Créer votre chemin $GOPATH. Ex: GOPATH=~/src/flossita
- Créer $GOPATH/src


# Comment utiliser le code de ce repo pour le tester?

Pour tester les exemple V0*, il suffit de lancer

```bash
./run.sh <repertoire version>
```

ex:

```bash
./run.sh v0.4
```

# Comment créer votre code à partir de ce repo?

la création d'un projet GO est très simple:


```bash
mkdir $GOPATH/src/<mon_projet>
```


Ex:
```bash
mkdir $GOPATH/src/debuter-en-go
```

Tous les répertoires v*/ contiennent du code go et peut être copié tel
quel dans votre `$GOPATH/src/<mon-projet>` pour test et autre.

A partir de là, un simple `go run`, voire (à partir de v1.*) `go build`
et `go install` marchera sans soucis.


Amusez vous bien.

C Larsonneur
